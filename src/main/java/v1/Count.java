package v1;

public class Count{
	private String name;
	private long amount;
	
	public Count(String name, long amount) {
		this.name = name;
		this.amount = amount;	
	}

	public String getName() {
		return name;
	}

	public long getAmount() {
		return amount;
	}
}