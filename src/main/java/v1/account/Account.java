package v1.account;

import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.util.Date;

@Entity  /*JPA Entity*/
@Table (name = "accounts")
@DiscriminatorColumn(name="accountType",discriminatorType=DiscriminatorType.STRING) 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Account {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE) //to use postgres sequences for generating an id
	private Long id;
	
	private String firstName;
	private String lastName;
	private String userName;
	private String password;
	private Date dateOfBirth;
	
	private Account() {}

	public Account(String firstName, String lastName, String userName, String password, Date dateOfBirth) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.password = password; //hash this
		this.dateOfBirth = dateOfBirth;
	}

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getLastName() {
		return lastName;
	}
}