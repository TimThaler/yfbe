package v1.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import v1.student.StudentRepository;
import v1.student.Student;

@RestController
@RequestMapping("studends/{studentID}")
public class StudentController {
	private final StudentRepository studentRepository;
	
	@Autowired
	public StudentController(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}
	
	 @PostMapping("/studends")
		public ResponseEntity<Student> create(
				@RequestBody Student student)
		{
			Student s =  this.studentRepository.save(new Student(
					student.getFirstName(),
					student.getLastName(),
					student.getUserName(),
					student.getPassword(),
					student.getDateOfBirth(),
					student.getUniversityName()
					)
					);
			return new ResponseEntity<Student>(s,HttpStatus.CREATED);
		}
}