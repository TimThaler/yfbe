package v1.student;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import v1.account.*;

@Entity
@Table(name="students")
@DiscriminatorValue("student")  
public class Student extends Account{
	
	@NotNull
	private String universityName;

	public Student() {
		super();
	}

	public Student(
			String firstName, 
			String lastName, 
			String userName, 
			String password, 
			Date dateOfBirth, 
			String universityName
			) {
		super(firstName, lastName, userName, password, dateOfBirth);
		this.universityName = universityName;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
}
