package v1;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import v1.Interfaces.Konstanten;

public class DatabaseConnector
implements v1.Interfaces.DatabaseConnector{
	Connection c = null;
	Statement stmt = null;
	DatabaseMetaData dmd = null;
	ResultSet rs = null;
	
	
	public DatabaseConnector(){		
		String postgresUser = null;
		String postgresUrl = null;
		String postgresPassword = null;
		
		Properties properties = new Properties();
		InputStream input = null;
		
		try{
			input = new FileInputStream("src/main/resources/config.properties");
			properties.load(input);
			
			Class.forName("org.postgresql.Driver");
			this.c = DriverManager
					.getConnection(properties.getProperty("dburl"),
							properties.getProperty("dbusername"),
							properties.getProperty("dbpassword"));
			System.out.println("[***] Opened database " +  properties.getProperty("dburl") + " successful");
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			System.exit(0);
		}finally {
			if(input != null) {
				try {
					input.close();
				} catch(Exception e) {
					e.printStackTrace();
					System.err.println(e.getClass().getName()+": "+e.getMessage());
					System.exit(0);
				}
			}
		}
	}
	
	 
	/**
	 * what is this function doing?
	 */
	public void clearTable( ) {
		String sql = "DELETE  FROM ";
        
		try {
			stmt = c.createStatement();
			stmt.executeUpdate(sql);
			System.out.println("[***] Table   cleared");
		} catch (Exception ex) {
			ex.printStackTrace();
			System.err.println(ex.getClass().getName()+": "+ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	 
	public boolean tableExists( ){		
		try {
			dmd = c.getMetaData();
			String tableName = "Test";//.toString().toLowerCase();
			ResultSet rs = dmd.getTables(null, null,"%", new String[] {"TABLE"});
			while (rs.next()){			
				if(rs.getString(3).equals(tableName)){
					System.out.println("[***] Table   exists");
					return true;
				}
			}
			System.out.println("[***] Table  does not exists");
			return false;
		}catch(SQLException e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			System.exit(0);
			}
		return false;
	}

	public boolean createUser(String name) {
		String sql = "INSERT INTO users (name) VALUES(" + name + ");";
		try {
			c.setAutoCommit(false);	
			stmt = c.createStatement();
 			stmt.executeQuery(sql);
 			return true;
		}catch (SQLException e) {
			e.printStackTrace();
		}
 
		return false;	 
	}
	 
	public void close()
	{
		try {								
			if(stmt != null) stmt.close();
			if(c != null) c.close();
			if(rs != null) rs.close();
			System.out.println("[***] Database Connector closed");
		}catch(Exception e) {}
	}
}
