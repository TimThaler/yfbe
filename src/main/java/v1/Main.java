package v1;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController 	/* Stereotype annotation*/				
public class Main {	/* Spring considers this class when handling incoming web requests*/

	@RequestMapping("/")	/* provides routing information*/							
	public String root() {	/* Request with root is mapped to method root*/
		return "Hello";		/* Spring renders the resulting String directly to the caller*/
	}
}
/*Both annotations belong to Spring MVC not specific to Spring boot*/