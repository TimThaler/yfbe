package v1.researcher; 

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import v1.account.Account; 


@Entity  /*JPA Entity*/
@Table (name = "researchers")
@DiscriminatorValue("researcher") 
public class Researcher extends Account{
	private String name;
	
	protected Researcher() {}
	
	public Researcher(Long id, String name) {
		 
		this.name = name;
	}
 
	
	public void setName(String name) {
		this.name= name;
	}

	public String getName() {
		return this.name;
	} 
}