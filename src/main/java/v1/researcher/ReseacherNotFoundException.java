package v1.researcher;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ReseacherNotFoundException extends RuntimeException{ 

	private static final long serialVersionUID = 1L;

	public ReseacherNotFoundException(String researcherID) {
		super("Cannot find researcher with id: " + researcherID);
	}
}

/*The serialization runtime associates with each serializable class a version number, 
 * called a serialVersionUID, which is used during deserialization to verify that the 
 * sender and receiver of a serialized object have loaded classes for that object that 
 * are compatible with respect to serialization. If the receiver has loaded a class for 
 * the object that has a different serialVersionUID than that of the corresponding sender's 
 * class, then deserialization will result in an InvalidClassException. A serializable class can 
 * declare its own serialVersionUID
 * explicitly by declaring a field named "serialVersionUID" that must be static, final, and of type long
 */