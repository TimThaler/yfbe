package v1.researcher;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface ReseacherRepository extends CrudRepository<Researcher, Long>{
	
	List<Researcher> findByLastName(String lastName);
	List<Researcher> findByFirstName(String firstName);
}

/*
 * CustomerRepository extends the CrudRepository interface. The type of entity and ID that it 
 * works with,Customer and Long, are specified in the generic parameters on CrudRepository. 
 * By extending CrudRepository, CustomerRepository inherits several methods for working 
 * with Customer persistence, including methods for saving, deleting, and finding Customer 
 * entities.
 * */