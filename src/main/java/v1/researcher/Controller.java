package v1.researcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity; /* Allows to modify request & response headers*/ 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;

import v1.ConnectionPoolManager;
import v1.Count;
import v1.DatabaseConnector;  

@RestController
@RequestMapping("researchers/{researcherID}")
public class Controller {	
	
	private final ReseacherRepository researcherRepository;
	
	@Autowired
	public Controller(ReseacherRepository researcherRepository) {
		this.researcherRepository = researcherRepository;
	}
	
    @GetMapping
	public ResponseEntity<Researcher> read( 
			@PathVariable Long researcherID)
	{
    	Researcher u = this.researcherRepository.findById(researcherID)
    			.orElseThrow(() -> new ReseacherNotFoundException(String.valueOf(researcherID)));
    	return new ResponseEntity<Researcher>(u,HttpStatus.OK);
    }
    
    @PostMapping("/researchers")
	public ResponseEntity<Researcher> create(
			@RequestBody Researcher researcher)
	{
		this.researcherRepository.save(researcher);
		return new ResponseEntity<Researcher>(researcher,HttpStatus.CREATED);
	}
    
    @DeleteMapping
   	public void destroy( 
   			@PathVariable Long researcherID)
   	{
       	this.researcherRepository.deleteById(researcherID);
   	}
	
	@RequestMapping(value="/countresearchers", method = RequestMethod.GET)
	public ResponseEntity<Count> countUsers()
	{
		return new ResponseEntity<Count>(
				new Count(this.getClass().getName(),this.researcherRepository.count()),
				HttpStatus.OK);
	}
	
	@RequestMapping("/researcherssgreeting")
	public ResponseEntity<Researcher> reseacherGreeting(
			@RequestParam(value="id") String id,
			@RequestParam(value="name") String name)
	{
		return new ResponseEntity<Researcher>(new Researcher(Long.valueOf(id),name),HttpStatus.OK);
	}
	
	
	

}

/*			@ModelAttribute("user") @Validated User user, //?			
			BindingResult bindingResult, Model model) throws Exception //?	 
	{	 	
		if (bindingResult.hasErrors()) {		
			throw new Exception("error");	     
		}
		
		model.addAttribute("name", user.getName()); 	     		
		//model.addAttribute("id", user.getId());	
		DatabaseConnector dbc = ConnectionPoolManager.getInstance().getDBCfromPool();	 
		return "result"; 
	}*/