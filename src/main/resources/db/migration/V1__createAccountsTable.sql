CREATE TABLE accounts(
	ID int PRIMARY KEY, 
	first_name varchar(255) not null,
	last_name varchar(255) not null,
	date_of_birth date not null
);
