ALTER TABLE accounts
	ADD COLUMN username varchar(50) NOT NULL UNIQUE,
	ADD COLUMN password varchar(70) NOT NULL;
